package javatest.component;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javatest.service.TestService;

@Component
public class FirstExecutor extends AbstractTestExecutor implements TestExecutor {
	
	public FirstExecutor() {
		super();
	}
	
	@Autowired
	public FirstExecutor(TestService testService) {
		this();
		setTestService(testService);
	}
	
	@PostConstruct
	public void init() {
		NAME = "FIRST";
		MESSAGE = "FIRST EXECUTE...";
	}
	
	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public String execute() {
		System.out.println(getTestService().getTestStr(NAME));
		System.out.println(MESSAGE);
		return NAME;
	}

}
