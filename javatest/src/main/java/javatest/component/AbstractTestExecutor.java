package javatest.component;

import javatest.service.TestService;

public abstract class AbstractTestExecutor {
	
	protected String NAME;
	
	protected String MESSAGE;
	
	private TestService testService;
	
	public TestService getTestService() {
		return this.testService;
	}
	
	public void setTestService(TestService testService) {
		this.testService = testService;
	}

}
