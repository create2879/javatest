package javatest.component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component	
public class TestFactory {

	@Autowired
	private List<TestExecutor> testExecutors;
	
	private Map<String, TestExecutor> testExecutorAsMap;
	
	@PostConstruct
	public void init() {
		testExecutorAsMap = new HashMap<>();
		testExecutors.forEach(executor -> testExecutorAsMap.put(executor.getName(), executor));
	}
	
	public TestExecutor getTestExecutor(String executorName) {
		return testExecutorAsMap.get(executorName);
	}
}
