package javatest.component;

public interface TestExecutor {

	public String getName();
	
	public String execute();
}
