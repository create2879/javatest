package javatest.component;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javatest.service.TestService;

@Component
public class SecondExecutor extends AbstractTestExecutor implements TestExecutor {
	
	public SecondExecutor() {
		super();
	}
	
	@Autowired
	public SecondExecutor(TestService testService) {
		this();
		setTestService(testService);
	}
	
	@PostConstruct
	public void init() {
		NAME = "SECOND";
		MESSAGE = "SECCOND SECOND EXECUTE...";
	}
	
	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public String execute() {
		System.out.println(getTestService().getTestStr(NAME));
		System.out.println(MESSAGE);
		return NAME;
	}

}
