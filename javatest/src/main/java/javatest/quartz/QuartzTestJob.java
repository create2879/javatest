package javatest.quartz;

import java.util.Date;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

@DisallowConcurrentExecution
public class QuartzTestJob implements Job {

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		try {
			Date date = new Date();
			System.out.println("JOB START : " + date);
			Thread.sleep(10000L);
			System.out.println("JOB END : " + date);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
