package javatest.quartz;

import javax.annotation.PostConstruct;

import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.stereotype.Component;

@Component
public class QuartzTestScheduler {
	
	private SchedulerFactory schedulerFactory;
    private Scheduler scheduler;

	@PostConstruct
	public void init() throws SchedulerException {
		schedulerFactory = new StdSchedulerFactory();
        scheduler = schedulerFactory.getScheduler();
        scheduler.start();
		
        JobDetail job = JobBuilder.newJob(QuartzTestJob.class).withIdentity("testJob").build();
        
        Trigger trigger = TriggerBuilder.newTrigger().withSchedule(CronScheduleBuilder.cronSchedule("0/3 * * * * ?")).build();
        
        scheduler.scheduleJob(job, trigger);
        
	}
}
