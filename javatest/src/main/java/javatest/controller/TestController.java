package javatest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javatest.domain.model.TestModel;
import javatest.service.TestFactoryService;
import javatest.service.TestService;

@RestController
public class TestController {

	@Autowired
	private TestService testService;
	
	@Autowired
	private TestFactoryService factoryService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ResponseEntity<String> welcomeJavaTest() {
		return ResponseEntity.ok().body("WELCOME JAVA TEST WORLD!!!");
	}

	@RequestMapping(value = "/test/builder/{type}", method = RequestMethod.GET)
	public ResponseEntity<TestModel> getModelByBuilder(@PathVariable String type) {
		return ResponseEntity.ok().body(testService.getModelByBuilder(type));
	}

	@RequestMapping(value = "/test/staticfactory/{type}", method = RequestMethod.GET)
	public ResponseEntity<TestModel> getModelByStaticFactory(@PathVariable String type) {
		return ResponseEntity.ok().body(testService.getModelByStaticFactory(type));
	}
	
	@RequestMapping(value = "/test/builder/get", method = RequestMethod.GET)
	public ResponseEntity<TestModel> getModelByBuilder2(@RequestParam String type) {
		return ResponseEntity.ok().body(testService.getModelByBuilder(type));
	}
	
	@RequestMapping(value = "/test/builder/post", method = RequestMethod.POST)
	public ResponseEntity<TestModel> getModelByBuilder3(@RequestParam String type) {
		return ResponseEntity.ok().body(testService.getModelByBuilder(type));
	}
	
	@RequestMapping(value = "/test/factory", method = RequestMethod.GET)
	public ResponseEntity<String> factory(@RequestParam String type) {
		return ResponseEntity.ok().body(factoryService.getFactory(type));
	}
	
	@RequestMapping(value = "/test/single", method = RequestMethod.GET)
	public ResponseEntity<Boolean> single() {
		return ResponseEntity.ok().body(testService.getBoolean());
	}
}
