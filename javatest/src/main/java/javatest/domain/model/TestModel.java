package javatest.domain.model;

public final class TestModel {

	private String a;

	private String b;

	private TestModel() {
	}

	// builder pattern
	TestModel(TestModelBuilder builder) {
		this.a = builder.getA();
		this.b = builder.getB();
	}

	// static factory method
	public static TestModel getInstanceByA(String a) {
		TestModel model = new TestModel();
		model.a = a;
		return model;
	}

	public static TestModel getInstanceByB(String b) {
		TestModel model = new TestModel();
		model.b = b;
		return model;
	}

	public static TestModel getInstanceByAll(String a, String b) {
		TestModel model = new TestModel();
		model.a = a;
		model.b = b;
		return model;
	}

	public String getA() {
		return a;
	}

	public String getB() {
		return b;
	}
}
