package javatest.domain.model;

public class TestModelBuilder {

	private String a;

	private String b;

	public String getA() {
		return a;
	}

	public String getB() {
		return b;
	}

	public static TestModelBuilder builder() {
		return new TestModelBuilder();
	}

	public TestModelBuilder a(String a) {
		this.a = a;
		return this;
	}

	public TestModelBuilder b(String b) {
		this.b = b;
		return this;
	}

	public TestModel build() {
		return new TestModel(this);
	}
}
