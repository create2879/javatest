package javatest.properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:datasource.properties")
public class DatabaseProperties {

	@Value("${db.url}")
	private String url;
	
	@Value("${db.driverClassName}")
	private String driverClassName;
	
	@Value("${db.userName}")
	private String userName;
	
	@Value("${db.password}")
	private String pasword;

	public String getUrl() {
		return url;
	}

	public String getDriverClassName() {
		return driverClassName;
	}

	public String getUserName() {
		return userName;
	}

	public String getPasword() {
		return pasword;
	}
}
