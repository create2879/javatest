package javatest.util;

import java.io.UnsupportedEncodingException;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class AESUtil {

	private static final String KEY = "abcdefghijklmnop";
	
	private static final String IV = "1234567890123456";
	
	private static SecretKeySpec getSecretKeySpec() {
		SecretKeySpec keySpec = null;
		try {
			byte[] keyBytes = KEY.getBytes("UTF-8");
			keySpec = new SecretKeySpec(keyBytes, "AES");
		} catch (UnsupportedEncodingException e) {
			System.out.println("SetSecretKeySpec failed.");
		}
		return keySpec;
	}
	
	public static byte[] encodeBytes(byte[] rawBytes) {
		SecretKeySpec keySpec = getSecretKeySpec();
		byte[] encrypted = null;
		try {
			Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding");
			cipher.init(Cipher.ENCRYPT_MODE, keySpec, new IvParameterSpec(IV.getBytes("UTF-8")));
			encrypted = cipher.doFinal(rawBytes);
		} catch (Exception e) {
			System.out.println("AES encode failed.");
		}
		return encrypted;
	}
	
	public static byte[] decodeBytes(byte[] encBytes) {
		SecretKeySpec keySpec = getSecretKeySpec();
		byte[] decrypted = null;
		try {
			Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding");
			cipher.init(Cipher.DECRYPT_MODE, keySpec, new IvParameterSpec(IV.getBytes("UTF-8")));
			decrypted = cipher.doFinal(encBytes);
		} catch (Exception e) {
			System.out.println("AES decode failed.");
		}
		return decrypted;
	}
}
