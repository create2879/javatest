package javatest.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import javatest.properties.DatabaseProperties;

@Configuration
@EnableTransactionManagement
public class DatasourceConfig {
	
	@Autowired
	private DatabaseProperties dbProperties;

	@Bean
	public HikariDataSource hikariDataSource() {
		return new HikariDataSource(this.hikariConfig());
	}
	
	@Bean
	public HikariConfig hikariConfig() {
		HikariConfig hc = new HikariConfig();
		hc.setJdbcUrl(dbProperties.getUrl());
		hc.setDriverClassName(dbProperties.getDriverClassName());
		hc.setUsername(dbProperties.getUserName());
		hc.setPassword(dbProperties.getPasword());
		return hc;
	}
	
}
