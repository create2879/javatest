package javatest.service;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

import org.springframework.stereotype.Service;

import javatest.domain.model.TestModel;
import javatest.domain.model.TestModelBuilder;
import javatest.util.AESUtil;

@Service
public class TestService {

	private boolean TEST;
	
	public TestModel getModelByBuilder(String type) {
		if ("a".equals(type)) {
			return TestModelBuilder.builder().a("a").build();
		} else if ("b".equals(type)) {
			return TestModelBuilder.builder().b("b").build();
		} else {
			return TestModelBuilder.builder().a("a").b("b").build();
		}
	}

	public TestModel getModelByStaticFactory(String type) {
		if ("a".equals(type)) {
			return TestModel.getInstanceByA("a");
		} else if ("b".equals(type)) {
			return TestModel.getInstanceByB("a");
		} else {
			return TestModel.getInstanceByAll("a", "b");
		}
	}
	
	public boolean getBoolean() {
		TEST = !TEST;
		return TEST;
	}
	
	public String getTestStr(String type) {
		return "TEST STRING : " + type + " !!!";
	}
	
	public void FileAESEncode(String fileInPath, String fileOutputPath) throws IOException {
		
		Files.deleteIfExists(Paths.get(fileOutputPath));
		
		FileChannel fileInChannel = FileChannel.open(Paths.get(fileInPath), StandardOpenOption.READ);
		FileChannel fileOutChannel = FileChannel.open(Paths.get(fileOutputPath), StandardOpenOption.WRITE, StandardOpenOption.CREATE);
		
		int bufferSize = 1024 * 8;
		if(bufferSize > fileInChannel.size()) {
			bufferSize = (int) fileInChannel.size();
		}
		ByteBuffer byteBuffer = ByteBuffer.allocate(bufferSize);
		
		int byteRead = fileInChannel.read(byteBuffer);
		
		int count = 0;
		while (byteRead != -1) {
			count++;
			byteBuffer.flip();
			
			byte[] b = byteBuffer.array();
			fileOutChannel.write(ByteBuffer.wrap(AESUtil.encodeBytes(b)));
			byteBuffer.clear();
			
			byteRead = fileInChannel.read(byteBuffer);
		}
		System.out.println(count);
	}
	
	public void FileAESDecode(String fileInPath, String fileOutputPath) throws IOException {
		
		Files.deleteIfExists(Paths.get(fileOutputPath));
		
		FileChannel fileInChannel = FileChannel.open(Paths.get(fileInPath), StandardOpenOption.READ);
		FileChannel fileOutChannel = FileChannel.open(Paths.get(fileOutputPath), StandardOpenOption.WRITE, StandardOpenOption.CREATE);
		
		int bufferSize = 1024 * 8;
		if(bufferSize > fileInChannel.size()) {
			bufferSize = (int) fileInChannel.size();
		}
		ByteBuffer byteBuffer = ByteBuffer.allocate(bufferSize);
		
		int byteRead = fileInChannel.read(byteBuffer);
		
		int count = 0;
		while (byteRead != -1) {
			count++;
			byteBuffer.flip();
			
			byte[] b = byteBuffer.array();
			fileOutChannel.write(ByteBuffer.wrap(AESUtil.decodeBytes(b)));
			byteBuffer.clear();
			
			byteRead = fileInChannel.read(byteBuffer);
		}
		System.out.println(count);
	}	
}
