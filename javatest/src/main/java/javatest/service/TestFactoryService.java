package javatest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javatest.component.TestExecutor;
import javatest.component.TestFactory;

@Service
public class TestFactoryService {
	
	@Autowired
	private TestFactory factory;

	public String getFactory(String type) {
		TestExecutor executor = factory.getTestExecutor(type);
		if(executor == null) {
			return "EMPTY EXECUTOR : " + type;
		}
		return executor.execute();
	}
}
