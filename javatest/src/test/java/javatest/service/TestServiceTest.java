package javatest.service;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousFileChannel;
import java.nio.channels.CompletionHandler;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import org.junit.Test;
import org.springframework.stereotype.Service;

import javatest.util.AESUtil;

@Service
public class TestServiceTest {

	@Test
	public void test() {
		String test = "abcd";
		
		// StringBuilder
		StringBuilder sb = new StringBuilder(test);
		sb.reverse();
		System.out.println(sb.toString());

		// stack
		Stack<String> a = new Stack<>();
		Stack<String> b = new Stack<>();
		for(char c : test.toCharArray()) {
			a.push(String.valueOf(c));
		}
		b = (Stack<String>) a.clone();
		String returnValue = "";
		
		while(true) {
			if(a.isEmpty()) {
				break;
			} else {
				returnValue += a.pop();
			}
		}
		System.out.println(returnValue);
		
		List<String> returnValue2 = new ArrayList<>();
		String returnValue3 = "";
		StringBuilder sb2 = new StringBuilder();
		b.forEach(s -> returnValue2.add(s));
		b.forEach(s-> {
			sb2.append(s);
		});
		
		System.out.println(sb2.toString());
	}
	
	@Test
	public void test2() {
		List<String> a = new ArrayList<>();
		List<String> b = new ArrayList<>();
		
		ArrayList<String> c = new ArrayList<>();
		//b = a;
		b = (List<String>) ((ArrayList<String>) a).clone();
		//b.addAll(a);
		
		a.add("111");
		a.add("222");
		a.add("333");
		
		System.out.println(b);
	}
	
	@Test
	public void FileTest() throws IOException {
		String filePath = "C:/test/test";
		int bufferSize = 1024 * 8;
		
		AsynchronousFileChannel afc = AsynchronousFileChannel.open(Paths.get(filePath), StandardOpenOption.READ);
		
		System.out.println("파일 사이즈 : " + afc.size());
		
		ByteBuffer byteBuffer = ByteBuffer.allocateDirect(bufferSize);
		long attachment = 0l;
		afc.read(byteBuffer, 0, attachment, new CompletionHandler<Integer, Long>() {
			@Override
			public void completed(Integer result, Long attachment) {
				// TODO Auto-generated method stub
			}

			@Override
			public void failed(Throwable exc, Long attachment) {
				// TODO Auto-generated method stub
			}
		});
	}
	
	@Test
	public void FileTest2() throws IOException {
		String filePath = "C:/test/test";
		String fileOutputPath = "C:/test/test_out";
		
		FileChannel fileInChannel = FileChannel.open(Paths.get(filePath), StandardOpenOption.READ);
		FileChannel fileOutChannel = FileChannel.open(Paths.get(fileOutputPath), StandardOpenOption.WRITE, StandardOpenOption.CREATE);
		
		int bufferSize = 1024 * 8;
		if(bufferSize > fileInChannel.size()) {
			bufferSize = (int) fileInChannel.size();
		}
		ByteBuffer byteBuffer = ByteBuffer.allocate(bufferSize);
		
		int byteRead = fileInChannel.read(byteBuffer);
		
		while (byteRead != -1) {
			byteBuffer.flip();
			
			byte[] b = byteBuffer.array();
			fileOutChannel.write(ByteBuffer.wrap(b));
			byteBuffer.clear();
			
			byteRead = fileInChannel.read(byteBuffer);
		}
		
	}
	
	@Test
	public void FileTestAESEncode() throws IOException {
		/*String filePath = "C:/test/test";
		String fileOutputPath = "C:/test/test_out_enc";*/
		
		String filePath = "C:/test/CentOS7.iso";
		String fileOutputPath = "C:/test/CentOS7_enc.iso";
		
		Files.deleteIfExists(Paths.get(fileOutputPath));
		
		FileChannel fileInChannel = FileChannel.open(Paths.get(filePath), StandardOpenOption.READ);
		FileChannel fileOutChannel = FileChannel.open(Paths.get(fileOutputPath), StandardOpenOption.WRITE, StandardOpenOption.CREATE);
		
		int bufferSize = 1024 * 8;
		if(bufferSize > fileInChannel.size()) {
			bufferSize = (int) fileInChannel.size();
		}
		ByteBuffer byteBuffer = ByteBuffer.allocate(bufferSize);
		
		int byteRead = fileInChannel.read(byteBuffer);
		
		int count = 0;
		while (byteRead != -1) {
			count++;
			byteBuffer.flip();
			
			byte[] b = byteBuffer.array();
			fileOutChannel.write(ByteBuffer.wrap(AESUtil.encodeBytes(b)));
			byteBuffer.clear();
			
			byteRead = fileInChannel.read(byteBuffer);
		}
		System.out.println(count);
	}
	
	@Test
	public void FileTestAESDecode() throws IOException {
		/*String filePath = "C:/test/test_out_enc";
		String fileOutputPath = "C:/test/test_out_dec";*/
		
		String filePath = "C:/test/CentOS7_enc.iso";
		String fileOutputPath = "C:/test/CentOS7_dec.iso";
		
		Files.deleteIfExists(Paths.get(fileOutputPath));
		
		FileChannel fileInChannel = FileChannel.open(Paths.get(filePath), StandardOpenOption.READ);
		FileChannel fileOutChannel = FileChannel.open(Paths.get(fileOutputPath), StandardOpenOption.WRITE, StandardOpenOption.CREATE);
		
		int bufferSize = 1024 * 8;
		if(bufferSize > fileInChannel.size()) {
			bufferSize = (int) fileInChannel.size();
		}
		ByteBuffer byteBuffer = ByteBuffer.allocate(bufferSize);
		
		int byteRead = fileInChannel.read(byteBuffer);
		
		int count = 0;
		while (byteRead != -1) {
			count++;
			byteBuffer.flip();
			
			byte[] b = byteBuffer.array();
			fileOutChannel.write(ByteBuffer.wrap(AESUtil.decodeBytes(b)));
			byteBuffer.clear();
			
			byteRead = fileInChannel.read(byteBuffer);
		}
		System.out.println(count);
	}	
}
